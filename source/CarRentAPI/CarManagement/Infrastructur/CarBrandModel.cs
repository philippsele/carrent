﻿using System;

namespace CarRentAPI.CarManagement.Infrastructur
{
    public class CarBrandModel
    {
        public Guid BrandId { get; set; }
        public string Brand { get; set; }
    }
}
